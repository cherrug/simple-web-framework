import { IUserProps } from '../interfaces';
import { User } from '../models/User';
import { View } from './View';

export class UserForm extends View<User, IUserProps> {
    protected template(): string {
        return `
            <div>
                <input class="name-input"/>
                <button class="set-name">Set name</button></button>
                <button class="set-age">Set Random Age</button>
                <button class="save">Save</button>
            </div>
        `;
    }

    protected eventsMap(): { [key: string]: () => void } {
        return {
            'click:.set-age': this.onRandomAge,
            'click:.set-name': this.onSetName,
            'click:.save': this.onSave,
        };
    }

    private onSetName = (): void => {
        const input = this.parent.querySelector('.name-input');

        if (input instanceof HTMLInputElement) {
            const name = input.value;
            this.model.set({ name });
        }
    };

    private onRandomAge = (): void => {
        const age = Math.floor(Math.random() * 100);
        this.model.set({ age });
    };

    private onSave = (): void => {
        this.model.save();
    };
}
