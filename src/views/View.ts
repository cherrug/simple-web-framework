import { Model } from '../models/Model';

export abstract class View<T extends Model<K>, K> {
    protected regions: { [key: string]: Element } = {};

    constructor(public parent: Element, public model: T) {
        this.model.on('change', () => {
            this.render();
        });
    }

    public render(): void {
        this.parent.innerHTML = '';
        const templateElement = document.createElement('template');
        templateElement.innerHTML = this.template();

        this.bindEvents(templateElement.content);
        this.mapRegions(templateElement.content);

        this.nestRegions();

        this.parent.appendChild(templateElement.content);
    }

    protected nestRegions(): void {
        return;
    }

    protected regionsMap(): { [key: string]: string } {
        return {};
    }

    protected abstract template(): string;

    protected eventsMap(): { [key: string]: () => void } {
        return {};
    }

    protected bindEvents(fragment: DocumentFragment): void {
        const eventMap = this.eventsMap();

        for (const key of Object.keys(eventMap)) {
            const [event, selector] = key.split(':');

            fragment
                .querySelectorAll(selector)
                .forEach((el) => el.addEventListener(event, eventMap[key]));
        }
    }

    private mapRegions(fragment: DocumentFragment): void {
        const regionsMap = this.regionsMap();

        for (const key of Object.keys(regionsMap)) {
            const selector = regionsMap[key];
            const element = fragment.querySelector(selector);
            if (element) {
                this.regions[key] = element;
            }
        }
    }
}
