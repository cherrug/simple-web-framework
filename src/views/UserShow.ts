import { IUserProps } from '../interfaces';
import { User } from '../models/User';
import { View } from './View';

export class UserShow extends View<User, IUserProps> {
    protected template(): string {
        return `
            <div>User name: ${this.model.get('name')}</div>
            <div>User age: ${this.model.get('age')}</div>
        `;
    }
}
