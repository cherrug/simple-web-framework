import { Collection } from '../models/Collection';

export abstract class CollectionView<T, K> {
    constructor(
        protected parent: Element,
        protected collection: Collection<T, K>,
    ) {}

    public render(): void {
        this.parent.innerHTML = '';

        const templateElement = document.createElement('template');

        for (const model of this.collection.models) {
            const itemParent = document.createElement('div');
            this.renderItem(model, itemParent);
            templateElement.content.appendChild(itemParent);
        }

        this.parent.appendChild(templateElement.content);
    }

    protected abstract renderItem(model: T, itemParent: Element): void;
}
