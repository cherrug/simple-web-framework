import { IUserProps } from '../interfaces';
import { User } from '../models/User';
import { CollectionView } from './CollectionView';
import { UserShow } from './UserShow';

export class UserList extends CollectionView<User, IUserProps> {
    protected renderItem(model: User, itemParent: Element): void {
        new UserShow(itemParent, model).render();
    }
}
