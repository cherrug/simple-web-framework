import axios, { AxiosResponse } from 'axios';

import { Eventing } from './Eventing';

export class Collection<T, U> {
    public models: T[] = [];
    public events: Eventing = new Eventing();

    constructor(private url: string, private deserialize: (json: U) => T) {}

    get on() {
        return this.events.on;
    }

    get trigger() {
        return this.events.trigger;
    }

    public fetch(): void {
        axios.get(this.url).then((res: AxiosResponse) => {
            res.data.forEach((el: U) => {
                const user = this.deserialize(el);
                this.models.push(user);
            });

            this.trigger('change');
        });
    }
}
