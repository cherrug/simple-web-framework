import { IAttributes, IEvents, ISync, IUserProps } from '../interfaces';
import { APISync } from './APISync';
import { Attributes } from './Attributes';
import { Collection } from './Collection';
import { Eventing } from './Eventing';
import { Model } from './Model';

const url = 'http://localhost:3000/users';

export class User extends Model<IUserProps> {
    public static build(attrs: IUserProps): User {
        return new User(
            new Attributes<IUserProps>(attrs),
            new Eventing(),
            new APISync<IUserProps>(url),
        );
    }

    public static buildCollection(): Collection<User, IUserProps> {
        return new Collection(url, User.build);
    }
}
