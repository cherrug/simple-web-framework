import { AxiosResponse } from 'axios';

import { IAttributes, IEvents, ISync } from '../interfaces';

interface IHasId {
    id?: number;
}

export abstract class Model<T extends IHasId> {
    public get = this.attributes.get;
    public on = this.events.on;
    public trigger = this.events.trigger;

    constructor(
        protected attributes: IAttributes<T>,
        protected events: IEvents,
        protected sync: ISync<T>,
    ) {}

    public set(update: T): void {
        this.attributes.set(update);
        this.trigger('change');
    }

    public fetch(): void {
        const id = this.get('id');

        if (typeof id !== 'number') {
            throw new Error('Cannot fetch without id');
        }

        this.sync
            .fetch(id)
            .then((props: AxiosResponse): void => this.set(props.data));
    }

    public save(): void {
        this.sync
            .save(this.attributes.getAll())
            .then((res: AxiosResponse): void => this.trigger('save'))
            .catch(() => this.trigger('error'));
    }
}
