import axios, { AxiosPromise, AxiosResponse } from 'axios';

interface IHasId {
    id?: number;
}

export class APISync<T extends IHasId> {
    constructor(public url: string) {}

    public fetch = (id: number): AxiosPromise<T> => {
        return axios.get(`${this.url}/${id}`);
    };

    public save = (data: T): AxiosPromise<T> => {
        const { id } = data;

        if (id) {
            // user exists in persistance
            return axios.put(`${this.url}/${id}`, data);
        } else {
            // brand new user
            return axios.post(this.url, data);
        }
    };
}
