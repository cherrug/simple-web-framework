import { AxiosPromise } from 'axios';
import { Callback } from './types';

export interface IUserProps {
    id?: number;
    name?: string;
    age?: number;
}

export interface IAttributes<T> {
    get<K extends keyof T>(key: K): T[K];
    set(update: T): void;
    getAll(): T;
}

export interface ISync<T> {
    fetch(id: number): AxiosPromise<T>;
    save(data: T): AxiosPromise<T>;
}

export interface IEvents {
    on(eventName: string, callback: Callback): void;
    trigger(eventName: string): void;
}

export interface IModel<T> {
    attributes: IAttributes<T>;
    sync: ISync<T>;
    events: IEvents;
}
