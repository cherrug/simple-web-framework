import { IUserProps } from './interfaces';
import { Collection } from './models/Collection';
import { User } from './models/User';
import { UserEdit } from './views/UserEdit';
import { UserList } from './views/UserList';

const user = User.build({ name: 'D', age: 40 });
const root = document.getElementById('root');
if (root) {
    new UserEdit(root, user).render();
}

const usersUrl = 'http://localhost:3000/users';

const users = new Collection<User, IUserProps>(usersUrl, (json: IUserProps) => {
    return User.build(json);
});

user.on('change', () => {
    const listRoot = document.getElementById('list');

    if (listRoot) {
        new UserList(listRoot, users).render();
    }
});

users.fetch();
