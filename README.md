# Simple web framework

This project has been done in order to better understand TypeScript composition and inheritance.

I use here models to handle events and sync to backend.
I use here views to render information to html and add event listeners.

This is a Backbone.js-like 😊
